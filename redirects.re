^keys.+$|keys/%s
^inkscape-man.+$|man/%s
^inkview-man.+$|man/%s
^(.+/tutorial-[a-z]+)\.html$|tutorials/%s.en.html
^.+/tutorial.+$|tutorials/%s
^keys/keys(\.[A-Za-z_]+)?.html$|keys/keys-1.4.x%s.html
^man/inkscape-man(\.[A-Za-z_]+)?.html$|man/inkscape-man-1.4.x%s.html
^man/inkview-man(\.[A-Za-z_]+)?.html$|man/inkview-man-1.4.x%s.html
